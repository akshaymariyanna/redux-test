import AuthorApi from '../api/mockAuthorApi';
import * as types from './actionTypes';
import {beginAsyncCall, asyncCallError} from './asyncStatusActions';

export function loadAuthorsSuccess(authors) {
  return { type: types.LOAD_AUTHORS_SUCCESS, authors};
}

export function loadAuthors() {
  return dispatch => {
    dispatch(beginAsyncCall());
    return AuthorApi.getAllAuthors().then(authors => {
      dispatch(loadAuthorsSuccess(authors));
    }).catch(error => {
      dispatch(asyncCallError);
      throw error;
    });
  };
}
