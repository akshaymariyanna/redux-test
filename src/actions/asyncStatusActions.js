import * as types from './actionTypes';

export function beginAsyncCall() {
  return {type: types.BEGIN_ASYNC_CALL};
}

export function asyncCallError() {
  return {type: types.ASYNC_CALL_ERROR};
}
