import {combineReducers} from 'redux';
import courses from './courseReducer';
import authors from './authorReducer';
import asyncCallsInProgress from './asyncStatusReducer';

const rootReducer = combineReducers({
  courses,
  authors,
  asyncCallsInProgress
});

export default rootReducer;
