import * as types from '../actions/actionTypes';
import initialState from './initialState';

function actionTypeEndsInSuccess(type) {
  return type.substring(type.length -8) == '_SUCCESS';
}

export default function asyncStatusReducer(state = initialState.asyncCallsInProgress, action) {
  if (action.type == types.BEGIN_ASYNC_CALL) {
    return state + 1;
  } else if (action.type == types.ASYNC_CALL_ERROR || actionTypeEndsInSuccess(action.type)) {
    return state - 1;
  }

  return state;
}
